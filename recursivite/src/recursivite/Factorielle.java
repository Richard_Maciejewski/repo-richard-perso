package recursivite;

import java.util.Scanner;

public class Factorielle {

	public static int factorielle(int number){
		int resultat = 0;
		if (number < 0){
			System.out.println("Mauvais param�tre");
		}
		if (number == 1) {
			return 1;
		}else {
			int sousResult = factorielle(number-1); 
			resultat = sousResult + number * number; 
			return resultat;
		}
	}
		
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Entrer une valeur : ");
		int number = sc.nextInt();
		int result = factorielle(number);
		System.out.println(result);
	}

}
