package recursivite;

import java.util.Scanner;

public class Fibonacci {

	public static int fibonacci(int number) {
		if (number == 0){
			return 0;
		}
		if (number == 1) {
			return 1;
		}else {
			return fibonacci(number - 1)+fibonacci(number -2);
		}
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrer un nombre : ");
		int number = sc.nextInt();
		int result = fibonacci(number);
		System.out.println(result);
		
//		switch (number) {
//		
//		case 1:
//			System.out.println("1. Suite de Fibonacci : ");
//			System.out.println("Entrer un nombre : ");
//			int result = fibonacci(number);
//			System.out.println(result);
//		
//		default:
//			break;
//		}
		
		
	}
	
}
