package recursivite;

public class Tableau {

	public static int sommeTableau(int[] tab,int i) {
		if(i>=tab.length) {
			return 0;
		}else {
			return tab[i]+sommeTableau(tab,i+1);
		}
	}

	public static void main(String[] args) {
		int tableauEntier[] = {12,1,2,3,4,5,6,7,80};

		System.out.println(sommeTableau(tableauEntier, 0));
	}

}
