package recursivite;

import java.util.Scanner;

public class Palindrome {

	
	public static boolean  estUnPalindrome(String str) {
		int i = str.length() - 1;
		boolean res = true;
		for (int a = 0; a < i && res; a++) {
			if (str.charAt(a) != str.charAt(i)) {
				res = false;
			}
			i--;
		}
		return res;
	}
	
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Saisir un mot : ");
		String mot = sc.nextLine();
		
		System.out.println(estUnPalindrome(mot));
	}

}
