package recursivite;

public class Somme {
	public static int sommeRecursive(int n) {
		if(n == 0) {
			return n;
		} else {
			return n + sommeRecursive(n-1);
		}
	}
	public static void main(String[] args) {
		int a = 4;
		System.out.println(sommeRecursive(a));
	}
}
