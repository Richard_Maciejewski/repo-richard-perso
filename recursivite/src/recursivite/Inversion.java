package recursivite;

import java.util.Scanner;

public class Inversion {

	public static int inverse(String str, int taille) {
		
		if(taille == 0) {
			return 0;
		}else {
			return (str.charAt(taille-1) - 48) + inverse(str, taille-1)*10;
		}
		
	}
	
	
	public static void main(String[] args) {
		String mot = "";
		Scanner sc = new Scanner(System.in);
		System.out.println("Saisir une chaine de caract�re : ");
		mot = sc.nextLine();
		int t = inverse(mot, mot.length());
		System.out.println("Le chiffre est : " + t);
	}

}
