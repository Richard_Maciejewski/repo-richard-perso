package modele;

public class Stagiaire {
	
	public static final int DUREE_STAGE = 6;
	
	private String nom;
	private int salaire;
	
	
	
	public String getNom() {
		return nom;
	}
	
	// Getter et Setter
	public int getSalaire() {
		return salaire;
	}
	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}
	
	
	public void nouveauSalaire(int salaire) {
		this.salaire=salaire;
	}

	public String toString() {
		return "Stagiaire [nom=" + this.nom + ", salaire=" + this.salaire + "]";
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
