package listes;
import java.util.Scanner;
import java.util.Set;
import java.util.ArrayList;
import java.util.Collections;

public class Ensemble {
	private int choix;
	String element;
	private ArrayList<String> liste;

	public Ensemble(){
		this.liste = new ArrayList<String>();
	}

	public void init() {
		Scanner sc = new Scanner (System.in);
		System.out.println("Veuillez saisir le nombre de chaines : ");
		choix = sc.nextInt();
		for (int i = 0; i < choix ; i++) {
			System.out.println("Veuillez saisir votre chaine " + i + " :");
			element = sc.next();
			liste.add(element);
		}
	}

	public Ensemble ordreCroissant() {
		Ensemble tri1 = new Ensemble();
		tri1.liste.addAll(this.liste);
		Collections.sort(tri1.liste);
		return tri1;
	}

	public Ensemble ordreCroissant2() {
		String temp="";
		Ensemble tri2 = new Ensemble();
		tri2.liste.addAll(this.liste);
		for (int j = 0; j < this.liste.size(); j++) {

			for(int i = 0 ; i < this.liste.size()-1 ; i++) {

				if(tri2.liste.get(i).compareTo(tri2.liste.get(i+1))>0){
					temp = tri2.liste.get(i);
					tri2.liste.set(i,tri2.liste.get(i+1) );
					tri2.liste.set(i+1, temp);
				}
			}		
		}
		return tri2;
	}

	public String toString() {
		String s="";
		if(this.liste.isEmpty()==true) {
			System.out.println("Ensemble vide");
		}else {
			s+=this.liste;
		}	
		return s;
	}

}