package annuaire;

public class Coordonnees {
	
	private String tel;
    private String adresse;
        
    public Coordonnees(String tel, String adresse) {
        this.tel = tel;
        this.adresse = adresse;
    }   
    
    public String getTel() {
        return tel;
    }
    public String getAdresse() {
        return adresse;
    }
    public String toString() {
        return "Coordonnees [tel=" + tel + ", adresse=" + adresse + "]";
    }

}
