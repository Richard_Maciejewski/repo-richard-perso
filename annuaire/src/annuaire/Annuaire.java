package annuaire;

import java.util.HashMap;
import java.util.Set;
public class Annuaire {
    private static HashMap<String, Coordonnees> annuaire = new HashMap<String, Coordonnees>();
    public Annuaire() { 
    }
        
    public String toString() {
        return "Annuaire []";
    }
    public void ajout(String n, Coordonnees c) {
        annuaire.put(n, c);
    }
    public void affichCoord(String n) {
        System.out.println(annuaire.get(n));
    }
    public void modifCoord(String n, Coordonnees c) {
        annuaire.replace(n, c);
    }
    
    public void suppression(String n) {
        annuaire.remove(n);
    }
    public void listerNoms() {
        Set<String> ensembleDesCoordonnees = annuaire.keySet();
        for (String coord : ensembleDesCoordonnees) {
            System.out.println(coord);
        }
    }
    public void listerTel() {
        Set<String> ensembleDesCoordonnees = annuaire.keySet();
        for (String coord : ensembleDesCoordonnees) {
            System.out.println(coord + " -- " + annuaire.get(coord).getTel());
        }
    }
    public void listerAdresse() {
        Set<String> ensembleDesCoordonnees = annuaire.keySet();
        for (String coord : ensembleDesCoordonnees) {
            System.out.println(coord + " -- " + annuaire.get(coord).getAdresse());
        }
    }
    public void affichAnnuaire() {
        Set<String> ensembleDesCoordonnees = annuaire.keySet();
        for (String coord : ensembleDesCoordonnees) {
            System.out.println(coord + " -- " + annuaire.get(coord));
        }
    }
}
