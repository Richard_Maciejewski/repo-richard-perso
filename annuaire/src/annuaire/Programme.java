package annuaire;

public class Programme {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
Annuaire annuaire1 = new Annuaire();
        
        annuaire1.ajout("Amaia", new Coordonnees("06", "adr1"));
        
        annuaire1.ajout("B", new Coordonnees ("06 07", "adr2"));
        
        annuaire1.ajout("Z", new Coordonnees ("07", "adr3"));
        
        annuaire1.affichAnnuaire();
        
        annuaire1.listerAdresse();
        
        annuaire1.listerNoms();
        
        annuaire1.affichCoord("Z");
        
        annuaire1.listerTel();
        
        annuaire1.suppression("Amaia");
        
        annuaire1.affichAnnuaire();
        
        annuaire1.modifCoord("B", new Coordonnees ("06 08", "adr3")); 
        
        annuaire1.modifCoord("Z", new Coordonnees ("07 01", "adr4"));
        
        annuaire1.affichAnnuaire();
    }
}


