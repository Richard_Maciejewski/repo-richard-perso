package exam;

public enum Mention {
	N("non Admis"),
	P("Passable"),
	AB("Assez Bien"),
	B("Bien"),
	TB("Tr�s Bien");
	
	private final String mention;


private Mention (String mention) {
		this.mention = mention;
}


public String getMention() {
	return mention;
}	
	
}