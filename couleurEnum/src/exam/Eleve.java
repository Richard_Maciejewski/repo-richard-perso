package exam;

public class Eleve {

	private String nom;
	private Double n1;
	private Double n2;
	private Double n3;
	private Double moyenne;
	private Mention resultat;

	public Eleve(String nom, Double n1, Double n2, Double n3) {
		this.nom = nom;
		this.n1 = n1;
		this.n2 = n2;
		this.n3 = n3;
		this.moyenne = (n1 + n2 + n3)/3; 

		if (moyenne < 10) {
			this.resultat = Mention.N;
		}else if(moyenne <= 10 && moyenne < 12) {
			this.resultat = Mention.P;
		}else if(moyenne <= 12 && moyenne < 14) {
			this.resultat = Mention.AB;
		}else if(moyenne <= 14 && moyenne < 16) {
			this.resultat = Mention.B;
		}else if(moyenne >= 16) {
			this.resultat = Mention.TB;			
		}
	}



	@Override
	public String toString() {
		return "Eleve : " + nom + "  | note1 = " + n1 + " | note 2= " + n2 + " | note3 = " + n3 + " | moyenne = " + moyenne + " |"
				+ "| resultat = " + resultat.getMention()  + "]";
	}



	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Double getN1() {
		return n1;
	}

	public void setN1(Double n1) {
		this.n1 = n1;
	}

	public Double getN2() {
		return n2;
	}

	public void setN2(Double n2) {
		this.n2 = n2;
	}

	public Double getN3() {
		return n3;
	}

	public void setN3(Double n3) {
		this.n3 = n3;
	}

	public Double getMoyenne() {
		return moyenne;
	}

	public void setMoyenne(Double moyenne) {
		this.moyenne = moyenne;
	}


}



