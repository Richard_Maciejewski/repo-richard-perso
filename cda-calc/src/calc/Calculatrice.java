package calc;

import java.util.Random;

public class Calculatrice {

	public static int addition(int parametreX, int parametreY) {
		System.out.println("coucou, c'est moi je suis laa");
		return parametreX + parametreY;
	}

	public static int soustraction(int parametreX, int parametreY) {
		return parametreX - parametreY;
	}

	public static int multiplication(int parametreX, int parametreY) {
		return parametreX * parametreY;
	}

	public static int max(int parametreX, int parametreY) {
		if (parametreX > parametreY) {
			return parametreX;
		} else {
			return parametreY;
		}
	}

	public static char signe(int parametreX) {
		if (parametreX > 0) {
			return '+';
		} else {
			return '-';
		}
	}

	public static long factorielle(int parametreX) {
//		int res = 1;
//		int i = parametreX;
//		while (i > 1) {
//			res = res * i;
//			i--;
//		}

		int res = 1;
		for (int i = 1; i <= parametreX; i++) {
			res = res * i;
		}
		return res;
	}

	public static boolean conjonction(boolean boolA, boolean boolB) {
		boolean res = boolA && boolB;
		return res;
	}

	public static boolean disjonction(boolean boolA, boolean boolB) {
		boolean res = boolA || boolB;
		return res;
	}

	public static boolean negation(boolean boolA) {
		boolean res = !boolA;
		return res;
	}

	public static int aleatoireInferieur(int max) {
		Random aleatoire = new Random();
		int nombreAleatoire = aleatoire.nextInt(max);
		System.out.println("Coucou");
		return nombreAleatoire;
	}
}
