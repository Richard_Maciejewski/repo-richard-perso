package calc;

import org.apache.commons.lang3.StringUtils;

public class MotsUtils {

	public static String inverser(String str) {
		
		return org.apache.commons.lang3.StringUtils.reverse(str);
	}

	// for (int i = str.length() - 1; i >= 0; i--) {
	// String newString = str.substring(0, str.length() - 1);
	// char str2 = str.charAt(str.length() - 1);
	// str = str2 + newString;
	// }
	// return str;
	// }
	// }

//	 public static String inverser(String str) {
//	 
//	 return StringUtils.reverse(String str);
//	 }
	 

//	public static String caracteresCommuns(String str, String str2) {
//		String res = "";
//		for (int i = str.length() - 1; i >= 0; i--) {
//			System.out.println("**************************** debut");
//			System.out.println("i = "+i);
//			System.out.println("res = "+res);
//			char c = str.charAt(i);
//			System.out.println("le caractere en cours  = "+c);
//			for (int j = str2.length() - 1; j >= 0; j--) {
//				System.out.println("   ---------------------- debut");
//				System.out.println("   j = "+j);
//				System.out.println("   res = "+res);
//
//				System.out.println(" index de "+c+" dans "+res+" est "+res.indexOf(c));
//				if (c == str2.charAt(j) && res.indexOf(c)==-1 ) {
//					res = res + str.charAt(i);
//				}
//				System.out.println("   ---------------------- fin");
//			}
//			System.out.println("**************************** fin");
//		}
//		return res;
//	}
//}
	public static String caracteresCommuns(String str, String str2) {
		String res = "";

		for (int i = 0; i < str.length(); i++) {

			char leCaractereEnCours = str.charAt(i);

			for (int j = 0; j < str2.length(); j++) {

				if (leCaractereEnCours == str2.charAt(j)) {

					int indexCaractereEnCourDansRes = res.indexOf(leCaractereEnCours);

					if (indexCaractereEnCourDansRes == -1) {
						res = res + leCaractereEnCours;
					}

				}

			}

		}
		return res;
	}

//	public static boolean estUnPalindrome(String a) {
//		return a.equals(MotsUtils.inverser(a));
//	}
//
//}

	public static boolean estUnPalindrome(String str) {
		int z = str.length() - 1;
		boolean res = true;
		for (int a = 0; a < z && res; a++) {
			if (str.charAt(a) != str.charAt(z)) {
				res = false;
			}
			z--;
		}
		return res;
	}

	public static int sommeChiffresDansMot(String str) {
		int res = 0;
		char[] tab = str.toCharArray();
		for (int i = 0; i < tab.length; i++) {
//			System.out.println("**************************** debut");
//			System.out.println("i = " + i);
//			System.out.println("res = " + res);
			if (Character.isDigit(tab[i])) {
				res = res + (tab[i] - 48);
			}
		}
		return res;
	}

//public static long sommeChiffresDansMot (String str) {
//
//	long som = 0;
//
//	char[] charArray = str.toCharArray();
//
//	for(char c : charArray) {
//		if (Character.isDigit(c)) {
//			som=som+Character.getNumericValue(c);
//		}
//	}
//	return som;	
//}

	public static String devinerAlgo(int param) {
		int param1;
		int param2;
		param1 = param * 2;
		System.out.println(param1);
		param2 = param - 1;
		System.out.println(param2);
		String s = "" + param1 + (param2);
		return s;
	}

//	public static String devinerAlgo(int param) {
//        return "" + 2*param + (param-1);
//    }

	public static void afficherNombreOccurence(String str) {
		String lettresDejaVues = "";
		for (int i = 0; i < str.length(); i++) {
			char laLettreEnCours = str.charAt(i);// lalettreEnCours = lettre que je traite
			if (lettresDejaVues.indexOf(laLettreEnCours) == -1) {
				int compteur = 0;// reinitialise le compteur pour le i suivant cad lettre suivante

				for (int j = 0; j < str.length(); j++) {
					if (laLettreEnCours == str.charAt(j)) {// si la lettre que je traite = lettre que je compare
						compteur = compteur + 1;// j'incremente le compteur
					}
				}
				System.out.println(laLettreEnCours + " --- " + compteur);
				lettresDejaVues = lettresDejaVues + laLettreEnCours;
//                System.out.println("lettresDejaVues : "+lettresDejaVues);
			}
		}
	}

	public static String exercice29(int param, int param2) {
		String s = "";
		if (param % 2 == 0) {
			param = param + 1;
		}
		s = s + param;
		for (int cpt = 1; cpt < param2; cpt++) {
			param = param + 2;
			s = s + "*" + param;
		}
		return s;
	}

	public static long sommeUnicodes(String str) {
		long a = 0;
		for (int i = 0; i < str.length(); i++) {
			a = a + str.charAt(i);
		}
		return a;
	}

//	long res = 0l;
//	for(int i = 0;i<str.length();i++){
//		res = res + str.charAt(i);
//		}
//	return res;

//	public static void main(String[] args) {
//        char monChar = 'v';
//        System.out.println("la valeur en caractere actuelle : "+monChar);
//        int deuxPasApresMonChar = monChar + 2;
//        System.out.println("deuxPasApresMonChar en nombre: "+deuxPasApresMonChar);
//        System.out.println("deuxPasApresMonChar en caractere : "+ (char)deuxPasApresMonChar);
//    }
	public static long conversionBinaire(String str) {
		int res = 0;
		int p = 1;
		for (int i = str.length() - 1; i >= 0; i--) {
			if (str.charAt(i) == '1') {
				res = res + p;
			} else if (str.charAt(i) != '0') {
			}
			p = p * 2;
		}
		return res;
	}

	public static String TED(String a) {
		int E = 0;
		int D = 0;
		if (a.indexOf('.') != -1) {
			for (int j = a.length() - 1; j >= 0; j--) {
				if (a.charAt(j) == '.') {
					break;
				}
				D = D + (int) a.charAt(j) - 48;
			}
		}
		for (int i = 0; i < a.length(); i++) {
			if (a.charAt(i) == '.') {
				break;
			}
			E = E + (int) a.charAt(i) - 48;
		}
		return "T" + (E + D) + "E" + (E) + "D" + (D);
	}

	public static String conversion(String str) {
		String res = "";
		String str2 = str.toLowerCase();
		for (int i = 0; i < str.length(); i++) {
			res = res + Integer.toString(str2.charAt(i) - 96);
		}
		return res;
	}

	public static String alphanumerique(String str) {
		String res = "";
		String a = "";
		int c = 0;
		for (int i = 0; i < str.length(); i++) {
			if (Character.isDigit(str.charAt(i))) {
				a = a + (str.charAt(i));
			} else if (str.charAt(i) != str.charAt(0) && !Character.isDigit(str.charAt(i))
					&& Character.isDigit(str.charAt(i - 1))) {
//				System.out.println(str.charAt(i));
				c = Integer.parseInt(a);
				for (int cpt = 0; cpt < c; cpt++) {
					res = res + str.charAt(i);
				}
				a = "";
			} else {
				res = res + str.charAt(i);
			}
		}
		return res;
	}

	public static void etoiles(int param) {
		while (param > 0) {
			for (int i = param; i > 0; i--) {
				System.out.print('*');
			}
			System.out.println();
			param--;
		}
	}
	
//	for (char c : tab) {
//	System.out.println(c);
//}

	public static int numerique(String str) {
		int i = 0;
		int nombre = 0;
		int res = 0;
		char[] tab = str.toCharArray();
		char signe = '+';
		String chiffre = "";
		
		while (i < tab.length) {
//*************************************************************************
			while (!Character.isDigit(tab[i])) {
				if (i == tab.length - 1) {
					if (signe == '+' && tab[i] == '+') {
						signe = '+';
					} else if (signe == '-' && tab[i] == '+') {
						signe = '-';
					} else if (signe == '-' && tab[i] == '-') {
						signe = '+';
					} else if (signe == '+' && tab[i] == '-') {
						signe = '-';
					}
					break;
				}

				if (signe == '+' && tab[i] == '+') {
					signe = '+';
				} else if (signe == '-' && tab[i] == '+') {
					signe = '-';
				} else if (signe == '-' && tab[i] == '-') {
					signe = '+';
				} else if (signe == '+' && tab[i] == '-') {
					signe = '-';
				}
				i++;
			}
//*********************************************************************
			while (Character.isDigit(tab[i])) {
				if (i == tab.length - 1) {
					chiffre = chiffre + tab[i];
					break;
				}
				chiffre = chiffre + tab[i];
				i++;
			}
//*************************************************************************			
			nombre = Integer.parseInt(chiffre);
			if (signe == '+') {
				res = res + nombre;
			} else {
				res = res - nombre;
			}

			chiffre = "0";
			signe = '+';
			if (i == tab.length - 1) {
				break;
			}
		}
		return res;
	}
}
