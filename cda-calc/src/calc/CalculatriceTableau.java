package calc;

public class CalculatriceTableau {

	public static int sommeElements(int[] tabInt) {
		int res = 0;
		for (int i = 0; i < tabInt.length; i++) {
			res = res + tabInt[i];
		}
		return res;
	}

	public static int plusPetitElement(int[] tabInt) {

		int res = tabInt[0];
		for (int i = 0; i < tabInt.length; i++) {

			if (res > tabInt[i]) {
				res = tabInt[i];
			}
		}
		return res;
	}

	public static int plusGrandElement(int[] tabInt) {
		int res = tabInt[0];
		for (int i = 0; i < tabInt.length; i++) {

			if (res < tabInt[i]) {
				res = tabInt[i];
			}
		}
		return res;
	}

	public static int sommeElementsDeuxTableaux(int[] tabInt, int[] tabInt2) {
		int som1 = 0;
		for (int i = 0; i < tabInt.length; i++) {
			som1 = som1 + tabInt[i];
		}
		int som2 = 0;
		for (int i = 0; i < tabInt2.length; i++) {
			som2 = som2 + tabInt[i];
		}
		int som = som1 + som2;
		return som;
	}

	public static int[] triAscendant(int[] tabInt) {
		// java.util.Arrays.sort(tabInt);
		int b = 0;
		int tab1[] = new int[tabInt.length];
		tab1 = tabInt.clone();
		for (int i = 0; i < tab1.length - 1; i++) {
			for (int j = i + 1; j < tab1.length; j++) {
				if (tab1[i] > tab1[j]) {
					b = tab1[i];
					tab1[i] = tab1[j];
					tab1[j] = b;
				}
			}
		}
		return tab1;
	}

//	public static int[] triAscendant(int[] tab) {
//		boolean test;
//		do {
//			test = false;
//			for (int i = 0; i < tab.length - 1; i++) {
//				if (tab[i] > tab[i + 1]) {
//					int a = tab[i];
//					tab[i] = tab[i + 1];
//					tab[i + 1] = a;
//					test = true;
//				}
//			}
//		} while (test);
//		return tab;
//	}

	public static boolean conjonction(boolean[] tabBool) {
		boolean f = true;
		for (int i = 0; f && i < tabBool.length; i++) {
			f = f && tabBool[i];
			f = f && tabBool[i];
		}
		return f;
	}

	public static long nombreDElementsPair(int[] tab) {
		int res = 0;
		for (int i = 0; i < tab.length; i++) {
			if (tab[i] % 2 == 0) {
				res = res + 1;
			}
		}
		return res;
	}

	public static boolean chercheSiUnElementExiste(int param, int[] tab) {
		for (int i = 0; i < tab.length; i++) {
			if (tab[i] == param) {
				return true;
			}
		}
		return false;
	}

	public static int[] mettreZeroDansLesCasesAIndicesImpair(int[] tab) {

		for (int i = 0; i < tab.length; i++) {
			if (i % 2 == 1) {
				tab[i] = 0;
			}
		}
		return tab;
	}

	public static int[] triAscendantDeuxTableaux(int[] tab1, int[] tab2) {
		int[] tabf;
		tabf = new int[tab1.length + tab2.length];

		for (int i = 0; i < tab1.length; i++) {
			tabf[i] = tab1[i];
		}

		for (int i = 0; i < tab2.length; i++) {
			tabf[i + tab1.length] = tab2[i];
		}
		return triAscendant(tabf);
	}

	public static int[] decalerLesElementsTableauDUneCase(int[] tab) {
		int a = tab.length - 1;
		int b = tab[a];
		for (int i = tab.length - 1; i > 0; i--) {
			tab[i] = tab[i - 1];
		}
		tab[0] = b;
		return tab;
	}
}
