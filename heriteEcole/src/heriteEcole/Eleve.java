package heriteEcole;

public class Eleve extends Personne {

		int cne;
		
		public Eleve (String prenom, String nom, int cne) {
			
			super(prenom, nom);
			this.cne = cne;
			cpt++;
			
			
		}

		@Override
		public int compareTo(Personne o) {
			if (o instanceof Professeur) {
				return -1;
			
			} else if (o instanceof Employe) {
				return 1; 
				
			} else {
				return (this.nom).compareTo(((Eleve) o).nom);
			}
		}
		
		@Override
		public String toString() {
			return "Je suis l'�tudiant "+super.toString()+" mon CNE est : " + cne;
		}

		public int getCne() {
			return cne;
		}

		public void setCne(int cne) {
			this.cne = cne;
		}
		
		
		
}
