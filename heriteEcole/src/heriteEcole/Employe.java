package heriteEcole;

public class Employe extends Personne {

		double salaire;
		
		public Employe (String prenom, String nom, double Salaire) {
			
			super (prenom, nom);
			this.salaire = salaire;
			cpt++;
		}

		@Override
		public String toString() {
			return "Je suis l'employ� " +super.toString()+" mon salaire est : "+ salaire+" euro";
		}

		public double getSalaire() {
			return this.salaire;
		}
		
		@Override
		public int compareTo(Personne o) {
			if (o instanceof Eleve) {
				return -1; 
			} else if (o instanceof Professeur) {
				return -2;
			} else {
				return this.nom.compareTo(((Employe) o).nom);
			}
		}

		
		
		
		
		
}
