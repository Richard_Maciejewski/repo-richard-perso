package heriteEcole;

public abstract class Personne implements Comparable<Personne> {
	
		private final int Id;
		protected static int cpt;
		protected static String prenom;
		protected static String nom;
		
		public Personne (String prenom, String nom){
			
			cpt++;
			this.Id=cpt;
			this.prenom = prenom;
			this.nom= nom;
			
		}
		
		

		@Override
		public String toString() {
			return "" + prenom + " " + nom;
		}

		public static String getNom() {
			return nom;
		}

		
		public static String getPrenom() {
			return prenom;
		}

		public int getId() {
			return Id;
		}
		
		
		
}
