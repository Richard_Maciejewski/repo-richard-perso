package heriteEcole;

import java.util.ArrayList;
import java.util.Collections;

public class main {

	static ArrayList<Personne> list = new ArrayList<Personne>();
	
	public static void main(String[] args) {

		Eleve el1 = new Eleve("Julien", "Chombart", 65678754);
		Eleve el2 = new Eleve("Sebastien", "Fremy", 87543543);
		Employe em3 = new Employe("Ambroise", "Delalin", 1900.0d);
		Employe em4 = new Employe("Francois", "Vandenplas", 1800.0d);
		Professeur p1 = new Professeur("Stili", "Oussama", 2000.0d, "JAVA/JEE");
		Professeur p2 = new Professeur("Thomas", "Savaton", 2000.0d, "Mathematiques");
		
		list.add(el1);
		list.add(el2);
		list.add(em3);
		list.add(em4);
		list.add(p1);
		list.add(p2);
		
		Collections.sort(list);
		System.out.println("apres tri : ************************");
		for (Personne personne : list) {
			System.out.println(personne);
		}
		
		
	}
		
}
