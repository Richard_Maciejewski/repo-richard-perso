package heriteEcole;

public class Professeur extends Employe{
		
	private String matiere;
	
	public Professeur (String prenom, String nom, double salaire, String matiere) {
		
		super(prenom, nom, salaire);
		this.matiere = matiere;
		this.salaire = salaire;
		cpt++;
	}
	
	@Override
	public int compareTo(Personne o) {
		if (o instanceof Eleve) {
			return 1; 
		} else if (o instanceof Professeur) {
			return this.prenom.compareTo(((Professeur) o).prenom);
		} else {
			return 2;
		}
	}

	@Override
	public String toString() {
		return "Je suis le professeur "+getPrenom()+" "+getNom()+" mon salaire est : "+getSalaire()+" ma spécilalité est : " + matiere;
	}
	
	

	public String getMatiere() {
		return matiere;
	}

	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}
	
	
	
}
