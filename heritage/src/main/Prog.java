package main;

import java.util.ArrayList;
import herite.*;

public class Prog {

	public static void main(String[] args) {

		ArrayList<Personne> listePersonne = new ArrayList<Personne>();

		Enseignant prof1 = new Enseignant("Titi", "bibi", 2500);
		Enseignant prof2 = new Enseignant("Mati", "mimi", 1800);

		Etudiant eleve1 = new Etudiant("tutu", "gege", 5);
		Etudiant eleve2 = new Etudiant("Kiki", "bob", 10);

		listePersonne.add(eleve1);
		listePersonne.add(eleve2);
		listePersonne.add(prof1);
		listePersonne.add(prof2);

		for (Personne personne : listePersonne) {
			if (personne instanceof Enseignant) {
				System.out.println(((Enseignant) personne).getSalaire() + " " + personne.getNom() + " " + personne.getPrenom() + " Enseignant");

			} else {
				System.out.println(((Etudiant) personne).getNiveau()  + " " + personne.getNom() + " " + personne.getPrenom() + " Etudiant");

			}

		}
	}

}
