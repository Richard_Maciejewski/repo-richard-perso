package herite;

public class Enseignant extends Personne {
	
	int salaire;
	
	public Enseignant(String nom, String prenom, int salaire) {
		
		super(nom, prenom);
		this.salaire = salaire;
		
	}

		
	@Override
	public String toString() {
		return "Enseignant [salaire=" + salaire + "]";
	}



	public int getSalaire() {
		return salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}
	
}
