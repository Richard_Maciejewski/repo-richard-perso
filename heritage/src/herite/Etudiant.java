package herite;

public class Etudiant extends Personne {
	
	private int niveau;
	
	

	public Etudiant(String nom, String prenom, int niveau) {
		
		super(nom, prenom);
		this.niveau = niveau;
	}
	
	

	@Override
	public String toString() {
		return "Etudiant [niveau=" + niveau + "]";
	}



	public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}
}
