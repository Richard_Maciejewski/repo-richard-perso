package geometrie;

import java.lang.Math;

public class Point {

	// Attributs d'un point-------------------------------------
	private double abscisse;
	private double ordonnee;
	private char nom;

	// Methodes
	// Deplacement d'un point
	public void translation(double deplace) {
	 
		abscisse = abscisse + deplace ; ordonnee = ordonnee + deplace;
		
	}
	
	//Distance entre 2 points
	public double distance (Point point) {
		
		return  Math.sqrt(Math.pow((point.getAbscisse() - this.abscisse),2) + Math.pow((point.ordonnee - this.getOrdonnee()),2));
		
	}
	
	//Afficher les r�sultats
	public void afficher() {
		System.out.println(  "le nom du point est : " + this.nom + " est : abscisse : " + this.abscisse + " ordonn�e : " + this.ordonnee);
		
	}
	
	//Symetrie d'un point
	public Point symetrique() {
		
		Point sym = new Point();
		sym.abscisse = this.abscisse*-1;
		sym.ordonnee = this.ordonnee*-1;
		
		return sym;
	}
	
	// Compare 2 points
	public boolean equals(Point point ) {
		
		boolean bool;
		if(point.abscisse == this.abscisse && point.ordonnee == this.ordonnee) {
			bool = true;
		}
		else { bool = false;
			
		}
		return bool;
	}
	// constructeurs------------------------------------------

	public Point() {
	}

	
	public Point(double abscisse, double ordonne, char nom) {
		this.abscisse = abscisse;
		this.ordonnee = ordonne;
		this.nom = nom;
	}

	// getters & setters

	public double getAbscisse() {
		return abscisse;
	}

	public void setAbscisse(double abscisse) {
		this.abscisse = abscisse;
	}

	public double getOrdonnee() {
		return ordonnee;
	}

	public void setOrdonnee(double ordonnee) {
		this.ordonnee = ordonnee;
	}

	public char getNom() {
		return nom;
	}

	public void setNom(char nom) {
		this.nom = nom;
	}

	

}