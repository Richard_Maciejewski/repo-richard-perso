package geometrie;

import java.util.Scanner;

public class ExecGeo {

	public static void main(String[] args) {

//		Point p1 = new Point(2., 5., 'a');
//		Point p2 = new Point(8., 8., 'b');
//
//		p2 = p1.symetrique();
//		p1.afficher();
//		System.out.println();
//		p2.afficher();
		
		Segment seg1 = new Segment(6., 4. , 'a');
		Segment seg2 = new Segment(4. , 7., 'b');
		Segment seg3 = new Segment(7., 4., 'c');
		Segment seg4 = new Segment(4., 6., 'd');
		
		seg1.afficherSegment();
		seg2.afficherSegment();
		
		
		Segment ressymetrie = seg1.symetriqueSegment();
		ressymetrie.afficherSegment();
		
		//seg1.tailleSegment();
		
		
		
	}

}
