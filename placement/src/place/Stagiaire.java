package place;

import java.util.ArrayList;
import java.util.List;
public class Stagiaire {
    // attributes
    private final String prenom;
    private final String nom;
    private final boolean head;
    public static List<Stagiaire> tousLesStagiaires = new ArrayList<>();
    // methods
    @Override
    public String toString() {
        if (head) {
            return "" + prenom + " " + nom;
        }
        return "" + prenom + " " + nom;
    }
    // getters & setters
    public String getPrenom() {
        return prenom;
    }
    public String getNom() {
        return nom;
    }
    public boolean isHead() {
        return head;
    }
    // constructors
    public Stagiaire(String prenom, String nom, boolean head) {
        this.prenom = prenom;
        this.nom = nom;
        this.head = head;
        tousLesStagiaires.add(this);
    }
}