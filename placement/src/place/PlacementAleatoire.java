package place;

import java.util.Random;

public class PlacementAleatoire {
	public static void main(String[] args) {
		String[] tab = { "Richard", "Anthony", "Linda", "Clement", "Amaia", "Yassine", "Ketsia", 
				"Badrane", "Soulaiman", "Laurent", "Mohamed", "Mustapha" };
		for (int i = tab.length - 1; i >= 1; i--) {
			// Echange aleatoire
			Random aleatoire = new Random();
			int nba = aleatoire.nextInt(tab.length);
			String s = tab[i];
			tab[i] = tab[nba];
			tab[nba] = s;
		}
		for (int j = 0; j < tab.length; j++) {
			int s = j + 1;
			System.out.println("A la place " + s + " : " + tab[j]);
		}

	}
}
