package triTreeSet;

import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;


public class TriTreeSet {

	public static void main(String[] args) {

		int entier = 0;
		String chaine = "";
		int a = 0;

		//TreeSet de type String
		TreeSet<String> str = new TreeSet<String>();
		//TreeSet de type Integer
		TreeSet<Integer> nb = new TreeSet<Integer>();

		Scanner sc = new Scanner(System.in);
		boolean bool = true;

		//Creation d'un Iterator pour parcourir le TreeSet


		System.out.println("***************************");
		//Ajout de String
		str.add("AUDI");
		str.add("BMW");
		str.add("PORSCHE");
		str.add("RENAULT");
		str.add("PEUGEOT");
		str.add("VOLKSWAGEN");
		str.add("VOLVO");
		str.add("FERRARI");
		str.add("FORD");
		str.add("FORD");

		//Creation d'un Iterator pour parcourir le TreeSet
		Iterator<String> ite = str.iterator();

		while(ite.hasNext()) {
			String s  = (String)ite.next();

			System.out.println(s);

		}
		
		System.out.println("***************************");
		System.out.println("notre TreeSet chaine : " + str);
		
	

		//Ajout de Integer
		nb.add(7);
		nb.add(5);
		nb.add(2);
		nb.add(1);
		nb.add(4);
		nb.add(3);
		nb.add(6);
		nb.add(6);

		System.out.println("notre TreeSet integer : " + nb);

		while (bool) {
			System.out.println(" 1. Ajouter un entier dans TreeSet : ");
			System.out.println(" 2. Ajouter une cha�ne dans TreeSet : ");
			System.out.println(" 3. Afficher les entiers saisis : ");
			System.out.println(" 4. Afficher les cha�nes de caract�res saisies : ");
			System.out.println(" 5. Supprimer un entier : ");
			System.out.println(" 6. Supprimer une chaine : ");
			System.out.println(" 7. Arr�ter le programme : ");
			entier = sc.nextInt();
			sc.nextLine();

			switch (entier) {

			case 1:
				System.out.println("Ajoutez un entier : ");
				a = sc.nextInt();
				nb.add(a);
				break;

			case 2:
				System.out.println("Ajoutez une cha�ne : ");
				chaine = sc.nextLine();
				str.add(chaine);
				break;

			case 3:
				System.out.println(nb);
				break;

			case 4:
				System.out.println(str);
				break;

			case 5:
				System.out.println("Supprimer un entier : ");
				entier = sc.nextInt();
				boolean existe = nb.remove(entier);
				System.out.println("Est-ce que l'entier " + entier + " existe ? " + existe);

			case 6:
				System.out.println("Supprimer une chaine : ");
				chaine = sc.nextLine();
				boolean exist = str.remove(chaine);
				System.out.println("Est-ce que la chaine " + chaine + " existe ? " + exist);
				break;

			case 7:
				System.out.println("Au revoir");
				bool = false;
				break;

			default:
				System.out.println("Num�ro incorrect");
				break;
			}

		}
		sc.close();

	}

}