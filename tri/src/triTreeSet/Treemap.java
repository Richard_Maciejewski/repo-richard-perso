package triTreeSet;

import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

public class Treemap {
    
	public static void main(String[] args) {
        
		TreeMap<Integer,String> tm = new TreeMap<Integer, String>();
        
		//tri des cl�s dans l'ordre
        tm.put(9, "blue");
        tm.put(8, "black");
        tm.put(7, "white");
        tm.put(6, "red");
        tm.put(5, "green");
        tm.put(4, "yellow");
        tm.put(3, "grey");
        tm.put(2, "purple");
        tm.put(1, "pink");
        tm.put(0, "orange");
        System.out.println(tm);
        System.out.println("***************************");
        //les cl�s n'ont pas de doublon
        tm.put(9, "blue");
        tm.put(6, "black");
        tm.put(7, "white");
        tm.put(6, "red");
        tm.put(3, "green");
        tm.put(8, "yellow");
        tm.put(3, "grey");
        tm.put(2, "purple");
        tm.put(5, "pink");
        tm.put(1, "orange");
        tm.put(0, null);
        
        System.out.println(tm);
        
        TreeMap<String, Integer> tm2 = new TreeMap<String, Integer>();
        
        //ajout des element de tm � tm2
        tm2.put("blue",0);
        tm2.put("black",1);
        tm2.put("white",2);
        tm2.put("red",3);
        tm2.put("green",4);
        tm2.put("yellow",5);
        tm2.put("grey",6);
        tm2.put("purple",7);
        tm2.put("purple",136);
        System.out.println("***********************");
        System.out.println(tm2);
        System.out.println("****************************");
        tm2.remove("grey");
        System.out.println(tm2);
        System.out.println("****************************");
        
         
        System.out.println("ma liste est de taille : " + tm2.size() );
        
        Iterator<String> ite = tm2.keySet().iterator();
        while(ite.hasNext()) {
            String str = ite.next();
            
            System.out.println("cl� : " + str + ", valeur : " + tm2.get(str));
        }
    }
}