package main;

public class Voiture {

	String marque;
	String model;
	int annee;
	
	public Voiture(String mark, String mod, int an) {
		
		this.marque = mark;
		this.model = mod;
		this.annee = an;
		
	}
	
	//la methode hashcode pour trier dans la keyset
	
	public int hashcode() {
		return annee;
	}
	
	//A revoir au niveau des conditions et des return, mais ca doit ressembler a ca.
	
	public boolean compareTo(Voiture b) {
		if(this.annee>b.annee)
			return false;
		if(this.marque.compareTo(b.marque) < 0)
			return false;
		if(this.model.compareTo(b.model) < 0)
			return false;
		return true;
		
	}
	
}
