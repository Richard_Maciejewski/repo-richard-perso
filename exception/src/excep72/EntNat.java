package excep72;

import java.util.Scanner;

public class EntNat {
    
	private int entier;
    
	public EntNat(int a) throws ErrConst {
        if (a < 0) {
            ErrConst e = new ErrConst(a);
            throw e;
        }
        this.entier = a;
    }

	@Override
	public String toString() {
		return "[ entier = " + entier + "]";
	}

	public int getN() {
        return this.entier;
    }
    
    public static EntNat somme(EntNat a, EntNat b) throws ErrSom, ErrConst {
        int c = a.getN();
        int d = b.getN();
        long res = (long) c + (long) d;
        if (res > Integer.MAX_VALUE) {
            throw new ErrSom(a.getN(), b.getN());
        }
        return new EntNat(c + d);
    }
    
    public static EntNat difference(EntNat a, EntNat b) throws ErrDiff, ErrConst {
        int c = a.getN();
        int d = b.getN();
        int res = c - d;
        if (res < 0) {
            throw new ErrDiff(a.getN(), b.getN());
        }
        return new EntNat(c - d);
    }
    
    public static EntNat multiplication(EntNat a, EntNat b) throws ErrProd, ErrConst {
        int c = a.getN();
        int d = b.getN();
        long res = ((long) c * (long) d);
        if (res > Integer.MAX_VALUE) {
            throw new ErrProd(a.getN(), b.getN());
        }
        return new EntNat(c * d);
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Saisissez un entier positif : ");
        
        try {
            EntNat a = new EntNat(sc.nextInt());
            System.out.println("Saisissez un entier positif : ");
            EntNat b = new EntNat(sc.nextInt());
            EntNat res = EntNat.somme(a, b);
            System.out.println("Somme de a + b : " + res);
            EntNat res2 = EntNat.difference(a, b);
            System.out.println("Différence entre a et b : " + res2);
            EntNat res3 = EntNat.multiplication(a, b);
            System.out.println("Multiplication de a X b : " + res);
            
        } catch (ErrNat e) {
            System.out.println(e.getMessage());
        }
        System.out.println("suite du programme ***************************");
        System.out.println("Saisissez un entier positif : ");
        try {
            EntNat a = new EntNat(sc.nextInt());
            System.out.println("Saisissez un entier positif : ");
            EntNat b = new EntNat(sc.nextInt());
            EntNat res = EntNat.somme(a, b);
            EntNat res2 = EntNat.difference(a, b);
            EntNat res3 = EntNat.multiplication(a, b);
        } catch (ErrConst | ErrSom | ErrDiff | ErrProd e) {
            System.out.println(e.getMessage());
        }
    }
}
