package excep72;

class ErrDiff extends ErrNat {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErrDiff(int entier, int entier2) {
        super("erreur ErrDiff : le r�sultat est n�gatif :  "+entier+" "+entier2);
    }
}
