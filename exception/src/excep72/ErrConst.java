package excep72;

class ErrConst extends ErrNat {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErrConst(int entier) {
        super("erreur ErrConst : l'entier saisi est n�gatif : "+entier);
    }
}