package excep;

public class EntNat {

	private final int entier;
	private EntNat(int entier) throws ErrConstante{
		if(entier < 0) {
			ErrConstante e = new ErrConstante(entier, "nombre negatif ");
			throw e;
		}else {
			this.entier = entier;
		}
	}

	public void getN(int i) throws ErrConstante {
		EntNat en1 = new EntNat(i);
	}
	@Override
	public String toString() {
		return "EntNat [entier=" + entier + "]coucou";
	}
	public static void main(String[] args)  throws ErrConstante {
		EntNat En = new EntNat(1);
		System.out.println(En);
		try {
			EntNat En1 = new EntNat(-5);
		}catch(ErrConstante E){
			System.out.println("Erreur : "+ E.getMessage());
		}
	}
}
