package excep;

public class ErrConstante extends Exception{
    
    
   	private static final long serialVersionUID = 1L;
	private final int entier;
    
    public ErrConstante(int entier,String mess) {
            super(mess);
            this.entier = entier;
    
    }
    
}