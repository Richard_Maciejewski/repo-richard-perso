package excep7;

public class EntNat  {
	private int num;

	public EntNat(int numb) throws ErrConst {
		if(numb<0 || numb>Integer.MAX_VALUE) {
			ErrConst e = new ErrConst(numb);
			throw e;
		}
		this.num=numb;
	}

	public int getN() {
		return this.num;
	}

	public static EntNat somme(EntNat a, EntNat b)throws ErrSom, ErrConst {
		EntNat som= new EntNat(a.getN()+b.getN());
		if(a.getN()+b.getN()<0) {
			ErrSom s = new ErrSom();
			throw s;
		}
		else {
			System.out.println("la somme est de "+som.num);
			return som;	
		}


	}



	public static EntNat diff(EntNat a, EntNat b)throws ErrDiff, ErrConst {
		if(a.num>b.num) {
			EntNat diff = new EntNat(a.getN()-b.getN());
			if(diff.num<0) {
				ErrDiff d = new ErrDiff();
				throw d;
			}
			else {
				System.out.println("la diff est de "+diff.num);
				return diff;	
			}


		}else{
			EntNat diff = new EntNat(b.getN()-a.getN());
			if(diff.num<0) {
				ErrDiff d = new ErrDiff();
				throw d;
			}
			else {
				System.out.println("la diff est de "+diff.num);
				return diff;	
			}
		}
	}





	public static EntNat produit(EntNat a, EntNat b)throws ErrProd, ErrConst {
		EntNat prod =new EntNat(a.getN()*b.getN());

		if(prod.num<0) {
			ErrProd p = new ErrProd();
			throw p;
		}
		else {
			System.out.println("le produit est de "+prod.num);
			return prod;	
		}

	}


	public static void main(String[] args) throws ErrConst, ErrSom, ErrDiff, ErrProd  {
		test();
		int test =0;
		test =2147483647*2147483644;
		System.out.println("******** "+ test);
	}

	public static void test() throws ErrConst {
		try {
			EntNat ent= new EntNat(12000);
			EntNat ent1 = new EntNat(1234525);
			EntNat.somme(ent, ent1);
			EntNat.diff(ent, ent1);
			EntNat.produit(ent, ent1);

		} 
		catch (ErrConst e) {
			System.err.println("l'entier qui pose probleme est "+e.getNum()+" "+e.getMessage());	
			throw e;
		}
		catch (ErrSom s) {
			System.err.println("Taille maxi d'un entier atteinte");	
		}
		catch (ErrDiff d) {
			System.err.println("Taille maxi d'un entier atteinte");	
		}
		catch (ErrProd p) {
			System.err.println("Taille maxi d'un entier atteinte");	
		}

		System.out.println("termine");
	}



	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
}
