package volaille;

import java.util.ArrayList;
import java.util.Scanner;

public abstract class Volaille {

	private final int id;
	private static int cpt;
	private String type;
	protected double poids;
	protected static int cptP;
	protected static int cptC;
	protected static int cptPa;


	public Volaille(String type, double poids) {

		cpt++;
		this.id = cpt;
		this.type = type;
		this.poids = poids;

		
	}




	@Override
	public String toString() {
		return "Volaille [Id = " + id + " , type = " + type + ", poids = " + poids + "]";
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}



	




	public static int getCpt() {
		return cpt;
	}

	public void setCpt(int cpt) {
		this.cpt = cpt;
	}

	public double getPoids() {
		return poids;
	}
	public void setPoids(double poids) {
		this.poids = poids;
	}
	public int getId() {
		return id;
	}

	public static int getCptP() {
		return cptP;
	}


	public void setCptP(int cptP) {
		this.cptP = cptP;
	}

	public int getCptC() {
		return cptC;
	}

	public void setCptC(int cptC) {
		this.cptC = cptC;
	}

	public int getCptPa() {
		return cptPa;
	}

	public void setCptPa(int cptPa) {
		this.cptPa = cptPa;
	}


}
