package volaille;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws Exception {

		boolean bool = true;
		Scanner sc = new Scanner(System.in);
		int entier ;
		ArrayList<Volaille> list = new ArrayList<Volaille>();

		double prix = 0;

		while (bool) {

			System.out.println("------------------------------------------");
			System.out.print("1 - Ajouter une volaille\r\n" 
					+ "2 - Vendre ou Rendre une volaille\r\n"
					+ "3 - Modifier le poids\r\n" 
					+ "4 - Modifier le prix\r\n"
					+ "5 - Afficher le nombre de volailles par type\r\n" + "6 - Total des prix d'abattage\r\n"
					+ "7 - Quitter\r\n");

			System.out.println("------------------------------------------");
			System.out.println("Que voulez-vous faire ?");
			entier = sc.nextInt();
			switch (entier) {

			case 1:
				// ajouterVolaille
				System.out.println("Saisir le type � ajouter  : 1. Poulet - 2. Canard - 3. Paon");
				entier = sc.nextInt();
				switch(entier) {
				case 1:
					System.out.println("Saisir le type");
					String repp = sc.next();
					System.out.println("Saisir son poids : ");
					double repp2 = sc.nextDouble();
					System.out.println("Saisir son prix : ");
					double repp3 = sc.nextDouble();
					Poulet.ajouterVolaille(repp, repp2, repp3,list);
					break;
				case 2:
					System.out.println("Saisir le type");
					String repc = sc.next();
					System.out.println("Saisir son poids : ");
					double repc2 = sc.nextDouble();
					System.out.println("Saisir son prix : ");
					double repc3 = sc.nextDouble();
					Canard.ajouterVolaille(repc, repc2, repc3, list);
					break;
				case 3:
					System.out.println("Saisir le type");
					String reppa = sc.next();
					System.out.println("Saisir son poids : ");
					double reppa2 = sc.nextDouble();
					Paon.ajouterVolaille(reppa, reppa2, list);
					break;
				}
				break;
				
			case 2:
				// vendre ou rendre volaille
				System.out.println("Liste des volailles : " + list);
				System.out.println("Quelle est la volaille � vendre ou � rendre : saisir le numero");
				int rep4 = sc.nextInt();
				Poulet.vendreVolaille(rep4, list);
				break;
			case 3:
				// modifPoids volaille
				System.out.println("Liste des volailles : " + list);
				System.out.println("Modifier le poids de quelle volaille : saisir le numero");
				int rep5 = sc.nextInt();
				System.out.println("Saisir le nouveau poids :");
				double po = sc.nextInt();
				Poulet.modifPoids(rep5, po, list);
				break;
			case 4:
				// modifPrix volaille (poulet canard)
				System.out.println("Liste des volailles : " + list);
				System.out.println("Modifier le prix de quelle volaille : saisir le numero");
				int rep6 = sc.nextInt();
				System.out.println("Saisir le nouveau prix :");
				double pr = sc.nextInt();
				Poulet.modifPoids(rep6, pr, list);
				break;
			case 5:
				// nombreVolaille par type (poulet canard paon)
				System.out.println("Liste des volailles par type : " + list);
				Poulet.listParType(list);
				break;
			case 6:
				// totalPrixAbattable (poulet canard)
				Poulet.totalPrixAbattable(list);
				break;
			case 7:
				// fin
				System.out.println("Merci, � bient�t.");
				bool = false;
				break;

			default:
				System.out.println("Num�ro incorrect");
				break;

			}
		}
	}

}
