package volaille;

import java.util.ArrayList;

public class Poulet extends Volaille {

	private double prix;

	public Poulet(String type, double poids, double prix) {

		super(type, poids);
		this.prix = prix;
		cptP++;

	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public static void ajouterVolaille(String rep, double rep2, double rep3 , ArrayList<Volaille> list) {

		if (list.size() < 7) {
			if(cptP < Stock.NB_POULET) {
				
				
				Poulet p = new Poulet(rep, rep2, rep3);
				list.add(p);
				System.out.println("La volaille a bien �t� ajout� !!!");
			}

		}

	}

	public static void vendreVolaille(int rep4,ArrayList<Volaille> list) {

		for (Volaille volaille : list) {

			if (cptP != 0 || cptC != 0 || cptPa != 0) {
				if (volaille.getId() == rep4) {
					if (volaille instanceof Poulet) {
						Poulet p = (Poulet) volaille;
						list.remove(p);
						System.out.println("Le poulet qui vaut " + p.getPoids() * p.getPrix() + " euros, a bien �t� vendu");
						
						break;
					}else if (volaille.getId() == rep4) {
						if (volaille instanceof Canard) {
							Canard c = (Canard) volaille;
							System.out.println(
									"Le Canard qui vaut " + (c.getPrix() * c.getPoids()) + " euros, a bien �t� vendu");
							list.remove(c);
							break;
						}

					}else if (volaille.getId() == rep4) {
						if (volaille instanceof Paon) {
							Paon Pa = (Paon)volaille;
							System.out.println("Le paon a �t� rendu");
							list.remove(volaille);
							break;
						}
					}

				}

			}
		}

	}

	public static void modifPoids (int rep5, double po,ArrayList<Volaille> list) {

		for (Volaille volaille : list) {

			if(volaille.getId() == rep5) {
				volaille.setPoids(po);
				System.out.println("Poids modifi�");
			}


		}
	}

	public static void modifPrix (int rep6, double pr,ArrayList<Volaille> list) {

		for (Volaille volaille : list) {

			if(volaille.getId() == rep6) {
				volaille.setPoids(pr);
				System.out.println("Prix modifi�");
			}


		}
	}

	public static void listParType (ArrayList<Volaille> list) {

		for (Volaille volaille : list) {
			int nombreP = 0;
			int nombreC = 0;
			int nombrePa = 0;

			if (volaille instanceof Poulet) {

				nombreP++;


			}else if (volaille instanceof Canard) {

				nombreC++;

			}else if (volaille instanceof Paon) {

				nombrePa++;

			}
			
			System.out.println("Nombre de poulets : " + nombreP);
			System.out.println("Nombre de canards : " + nombreC);
			System.out.println("Nombre de paons : " + nombrePa);
			break;
		
		}
		
	}

	public static void totalPrixAbattable(ArrayList<Volaille> list) {
		
		for (Volaille volaille : list) {

			if (cptP != 0 || cptC != 0 || cptPa != 0) {
				int nombreP = 0;
				int nombreC = 0;
				int nombrePa = 0;
				double poidsP = 0;
				double poidsC = 0;
				double prixttP = 0;
				double prixttC = 0;

				if (volaille instanceof Poulet) {
					Poulet p = (Poulet)volaille;
					nombreP++;
					poidsP = poidsP + volaille.getPoids();
					 
					prixttP = poidsP * p.getPrix() ;
						


			}else if (volaille instanceof Canard) {
				Canard c = (Canard)volaille;
				nombreC++;
				poidsC = poidsC + volaille.getPoids();
				prixttC=poidsC * c.getPrix();

			}else if (volaille instanceof Paon) {

				nombrePa++;
				
			}
				
				System.out.println("Poulets : " + prixttP + " euros.");
				System.out.println("Canards : " + prixttC + " euros.");
				System.out.println("Il y a " + nombrePa + " � rendre.");
		}
		}
		}
}



