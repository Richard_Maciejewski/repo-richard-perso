package code;

import java.util.*;
import java.io.*;
import java.math.*;
/**
* Auto-generated code below aims at helping you parse
* the standard input according to the problem statement.
**/
class Solution {
   public static void main(String args[]) {
       Scanner in = new Scanner(System.in);
       String message = in.nextLine();
       String answer ="";
       StringBuilder conversionBinaire = new StringBuilder();
       char[] messageChar = message.toCharArray();
       for (int i=0;i< messageChar.length;i++ ){
           String str = Integer.toBinaryString(messageChar[i]);
           while (str.length() != 7){
              str = '0' + str;
           }
           conversionBinaire.append(str);
       }
       int i = 0;
       char caractereEnCours;
       while (i < conversionBinaire.length())
       {
           if (conversionBinaire.charAt(i) == '0')
           {
               System.out.print("00 ");
               
               
               caractereEnCours = '0';
           }
           else
           {
               System.out.print("0 ");
               caractereEnCours = '1';
           }
           
           while (conversionBinaire.charAt(i) == caractereEnCours)
           {
               System.out.print("0");
               i++;
               if(i == conversionBinaire.length())
                   break;
           }
           
           if (i < conversionBinaire.length())
               System.out.print(" ");
       }
       // Write an action using System.out.println()
       // To debug: System.err.println("Debug messages...");
       System.out.println(answer);
   }
}