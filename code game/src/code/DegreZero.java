package code;

import java.util.Scanner;

public class DegreZero {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
        int res = 0;
        int n = in.nextInt(); // the number of temperatures to analyse
        if(n==0){
             System.out.println(0);
        } else {
            int[] tab = new int[n];
            for (int i = 0; i < n; i++) {
                
                int t = in.nextInt(); // a temperature expressed as an integer ranging from -273 to 5526
            
                tab[i] = t;
                
                if (i == 0) {
                res = t;
                 }
            }
            
            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages..."); 
    
            System.out.println(plusPetitElementProcheDeZero(tab));
        }
    }
    
    public static int plusPetitElementProcheDeZero(int[] tabInt) {

		int res = tabInt[0];
		for (int i = 0; i < tabInt.length; i++) {
			
			//Prend la valeur absolue des nombres
			if (Math.abs(res) > Math.abs(tabInt[i])) {
				res = tabInt[i];
			} else if(Math.abs(res) == Math.abs(tabInt[i]) ){
			    if(res < tabInt[i]){
			        res = tabInt[i];
			    }
			}
		
		}
		return res;
		
	}

}
