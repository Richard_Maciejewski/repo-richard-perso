package geometrie;
import java.lang.Math;
import java.util.Scanner;

public class Point {
	Scanner sc = new Scanner(System.in);
	private double abscisse;
	private double ordonnee;
	private char nom;
	
	public static void initialisationPoint(Point p) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Veuillez entrer un abscisse");
		p.abscisse = sc.nextDouble();

		System.out.println("Veuillez entrer un ordonn�e");
		p.ordonnee = sc.nextDouble();

		System.out.println("Veuillez entrer un nom de point");
		p.nom =sc.next().charAt(0);
	}
	public double getAbscisse() {
		return abscisse;
	}

	public double getOrdonnee() {
		return ordonnee;
	}

	public void afficher() {
		System.out.println("le point � pour nom "+this.nom+" pour abscisse "+this.abscisse+" pour ordonn�e "+this.ordonnee);
	}
	

	public void translation(double d) {
		abscisse = d+abscisse;
		ordonnee = d+ordonnee;
	}
	
	public double distance(Point param) {
		double distance = 0;
		distance = Math.sqrt(Math.pow((param.getAbscisse()-this.getAbscisse()),2) + Math.pow((param.getOrdonnee()-this.getOrdonnee()),2));
		return distance;
	}

	public Point symetrique() {
		Point psym = new Point(); 
		psym.abscisse = this.getAbscisse()*-1;
		psym.ordonnee = this.getOrdonnee()*-1;
		return psym;
	}

	public boolean equals(Point param) {
		boolean bool = false;
		if(param.getAbscisse()== this.getAbscisse() && param.getOrdonnee() == this.getOrdonnee()) {
			bool = true;		
		}
		return bool;
	}
}





