package geometrie;

import java.util.Scanner;

public class Senario {
	public static void testPoint() {
		Scanner sc = new Scanner(System.in);
		Point p = new Point(); 

		Point.initialisationPoint(p);

		p.afficher();

		System.out.println("Veuillez entrer un coefficient de translation pour X et Y");
		p.translation(sc.nextDouble());
		p.afficher();


		System.out.println("*********************************************************");
		System.out.println("nouveau point");

		Point p1 = new Point();
		Point.initialisationPoint(p1);
		p1.afficher();

		System.out.println("Veuillez entrer un coefficient de translation pour X et Y");
		p1.translation(sc.nextDouble());
		p1.afficher();


		double retourDeUsine = p.distance(p1);
		System.out.println("La distance entre les deux points est de "+retourDeUsine);

		Point psym = p.symetrique();
		System.out.println("Le point symetrique a pour Abscisse "+psym.getAbscisse() + " et pour Ordonn�e "+ psym.getOrdonnee());

		boolean retourDeEquals = p.equals(p1);
		System.out.println(retourDeEquals);
		sc.close();
	}

	
	public static void testSegment() {
		Scanner sc = new Scanner(System.in);
		Segment s = new Segment(); 

		Segment.initialisationSegment(s);

		s.afficher();

		double retourDeTaille = s.taille();
		System.out.println("La taille du Segment est de "+ retourDeTaille);

		System.out.println("Veuillez entrer un coefficient de translation pour le Segment");
		s.translate(sc.nextDouble());
		s.afficher();

		Segment sym = s.symetrique();
		System.out.println("Apr�s sym�trie");
		sym.afficher();

		Segment s1 = new Segment(); 
	
		Segment.initialisationSegment(s1);
		s1.afficher();
		boolean retourDeEqualsSeg = s.equals(s1);
		System.out.println(retourDeEqualsSeg);
	}

	public static void testTriangle() {
		Scanner sc = new Scanner(System.in);
		Triangle t = new Triangle();
		Triangle.initialisationTriangle(t);
		t.afficher();
		boolean retourIsocele =t.isocele();
		System.out.println(retourIsocele);
		boolean retourRectangle = t.rectangle();
		System.out.println(retourRectangle);
		double retourSurface =t.surface();
		System.out.println(retourSurface);
}
	
}
