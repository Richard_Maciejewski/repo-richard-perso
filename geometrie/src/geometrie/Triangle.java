package geometrie;
import java.lang.Math;
public class Triangle {
	Point a;
	Point b;
	Point c;

	public Triangle() {
		a = new Point();
		b = new Point();
		c = new Point();
	}

	public Point getA() {
		return a;
	}

	public Point getB() {
		return b;
	}

	public Point getC() {
		return c;
	}

	public static void initialisationTriangle( Triangle t) {
		System.out.println("initisalisation du triangle : ");
		Point.initialisationPoint(t.a);
		Point.initialisationPoint(t.b);
		Point.initialisationPoint(t.c);
	}
	public void afficher() {
		a.afficher();
		b.afficher();
		c.afficher();
	}

	public boolean isocele() {
		boolean bool = false;
		if(a.distance(b) == a.distance(c) || a.distance(c) == b.distance(c) || b.distance(c)== b.distance(a))
			bool = true;
		return bool;

	}

	public boolean rectangle() {
		boolean bool = false;
		if(Math.pow(a.distance(b), 2) == Math.pow(a.distance(c), 2) + Math.pow(b.distance(c), 2) ) {
			bool = true;
		}
		if(Math.pow(a.distance(c), 2) == Math.pow(a.distance(b), 2) + Math.pow(b.distance(c), 2) ) {
			bool = true;
		}
		if(Math.pow(b.distance(c), 2) == Math.pow(a.distance(b), 2) + Math.pow(a.distance(c), 2) ) {
			bool = true;
		}


		return bool;
	}
	
	public double surface() {
		double surface = 0;
		double max =0;
		double min = 0;
		if(rectangle()) {
			if(a.distance(b) > a.distance(c)) {
				max = a.distance(b);
				min = a.distance(c);
			} else {
				max = a.distance(c);
				min = a.distance(b);
			}
			
			if(b.distance(c)>max) {
				surface = (max * min)/2;
			}
			
		}
		
		if(isocele()) {
			double d=0;
			double z =0;
			if(a.distance(b) == a.distance(c)) {
				d = b.distance(c)/2;
				z = Math.pow(a.distance(b), 2)- Math.pow(d, 2);
				surface = (d*z)/2;
	
			}
			if(a.distance(b) == b.distance(c)) {
				d = a.distance(c)/2;
				z = Math.pow(a.distance(b), 2)- Math.pow(d, 2);
				surface = (d*z)/2;
		}
			if(b.distance(c) == a.distance(c)) {
				d = a.distance(b)/2;
				z = Math.pow(a.distance(c), 2)- Math.pow(d, 2);
				surface = (d*z)/2;
			}
			
	}
		return surface;
}
	
	public boolean equals(Triangle param) {
		boolean bool = false;
		if(this.a.equals(param.a) && this.b.equals(param.b) || this.b.equals(param.a) && this.a.equals(param.b) || this.c.equals(param.c)){
			bool = true;		
		}
		return bool;
	}

}
