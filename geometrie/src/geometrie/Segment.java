package geometrie;

import java.util.Scanner;

public class Segment {
	Point a;
	Point b;

	public Segment() {
		a = new Point();
		b = new Point();
	}
	
	public Point getA() {
		return a;
	}
	public Point getB() {
		return b;
	}
	
	public static void initialisationSegment(Segment s) {
		System.out.println("initisalisation du segment : ");
		Point.initialisationPoint(s.a);
		Point.initialisationPoint(s.b);
	}
	public void afficher() {
		System.out.println("le segment commence aux coordonn�es abscisse "+a.getAbscisse() + " pour ordonn�e "+a.getOrdonnee()+" le segment termine aux coordonn�es abscisse "+b.getAbscisse()+ " pour ordonn�e" +b.getOrdonnee());
	}
		public double taille () {
		double taille = 0;
		taille = Math.sqrt(Math.pow((a.getAbscisse()-b.getAbscisse()),2) + Math.pow((a.getOrdonnee()-b.getOrdonnee()),2));
		return taille;
	}
	
	public void translate(double d) {
		a.translation(d);
		b.translation(d); 
	}
	
	public Segment symetrique() {
		Segment sym = new Segment();
		sym.a = this.a.symetrique();
		sym.b = this.b.symetrique();
		return sym;
	}
	
	public boolean equals(Segment param) {
		boolean bool = false;
		if(this.a.equals(param.a) && this.b.equals(param.b) || this.b.equals(param.a) && this.a.equals(param.b)){
			bool = true;		
		}
		return bool;
	}
	
}



