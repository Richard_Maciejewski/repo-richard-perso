package jeu;

public final class Rank {

		
		public final static Rank rank2 = new Rank("2");
		public final static Rank rank3 = new Rank("3");
		public final static Rank rank4 = new Rank("4");
		public final static Rank rank5 = new Rank("5");
		public final static Rank rank6 = new Rank("6");
		public final static Rank rank1 = new Rank("AS");
		public final static Rank rank13 = new Rank("ROI");
		public final static Rank rank12 = new Rank("DAME");
		public final static Rank rank11 = new Rank("VALET");
		public final static Rank rank10 = new Rank("10");
		
		private final String valeur;
		
		private Rank(String valeur) {
		this.valeur = valeur;
		}

		@Override
		public String toString() {
			return "Rank [valeur=" + valeur + "]";
		}
		
}
