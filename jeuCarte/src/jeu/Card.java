package jeu;

public final class Card {

	private final Suit suit; //couleur
	private final Rank rank; //valeur
	
	public Card(Suit suit, Rank rank) {
		this.suit = suit;
		this.rank = rank;
	}

	
	@Override
	public String toString() {
		return "Card [suit=" + suit + ", rank=" + rank + "]";
	}

	

}
