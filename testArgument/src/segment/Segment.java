package segment;

public class Segment {
	
	private int a;
	private int b;
	
	
	// Initialisation du segment
	public Segment(int a, int b) {
		
		this.a = a;
		this.b = b;
		
	}

	public String toString() {
		
		return "Segment";
	}
	
	public boolean appartient(int point) {
		boolean bool = false;
		int min =0;
		int max =0;
		if(this.getA()>this.getB()) {
			min = this.getB();
			max = this.getA();
		}else {
			min = this.getA();
			max = this.getB();
		}
		if( point <min || point >max) {
			bool = false;
		}else {
			bool = true;
		}
			
		
		
		return bool;
	}
	
	public int longueur() {
		
		return this.getA()-this.getB();
	}
	
	
	
	// Getters et Setters
	public int getA() {
		return a;
	}


	public void setA(int a) {
		this.a = a;
	}


	public int getB() {
		return b;
	}


	public void setB(int b) {
		this.b = b;
	}

	
	

}
