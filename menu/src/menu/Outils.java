package menu;

import java.util.ArrayList;


public class Outils {

	private ArrayList<String> chaine = new ArrayList<String>();
	private ArrayList<Integer> nombre = new ArrayList<Integer>();

	public Outils() {
		this.chaine = new ArrayList<String>();
	}

	public void ajouteChaine(String a) {
		if (this.chaine == null) {
			System.out.println("Vous n'avez rien saisi !!!");
		} else {
			this.chaine.add(a);
		}
	}

	public void supprimeChaine(String a) {
		if (this.chaine.contains(a)) {
		} else {
			this.chaine.remove(a);
		}
	}

	public void afficheChaine() {
		if (chaine != null) {
			System.out.println("La chaine est vide");
		} else {
			System.out.println("La chaine comprend : " + this.chaine);
		}
	}

	public ArrayList<String> getChaine() {
		return chaine;
	}

	public void setChaine(ArrayList<String> chaine) {
		this.chaine = chaine;
	}

	public ArrayList<Integer> getNombre() {
		return nombre;
	}

	public void setNombre(ArrayList<Integer> nombre) {
		this.nombre = nombre;
	}

	public void ajouteNombre(int nb) {
		if (this.getNombre() == null) {
			System.out.println("Vous n'avez pas saisi un nombre");

		} else {
			this.nombre.add(nb);
			System.out.println("La liste des entiers comprend : " + this.nombre.add(nb));
		}
	}

	public void supprimeNombre(int nb) {
		if (this.nombre.contains(nb)) {
		} else {
			this.nombre.remove(nb);
		}
	}

	public void afficheNombre() {
		if (nombre != null) {
			System.out.println("La liste des entiers est vide");
		} else {
			for (int i = 0; i < nombre.size(); i++) {
				System.out.println("La liste des entiers comprend : " + nombre.get(i));
			}
		}

	}
}
