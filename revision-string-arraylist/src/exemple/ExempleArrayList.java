package exemple;

import java.util.ArrayList;
import java.util.Arrays;

public class ExempleArrayList {

	public static void main(String[] args) {

		ArrayList<String> maListe2 = new ArrayList<String>();

		maListe2.add("Linda");
		maListe2.add("Clement");
		maListe2.add("Radouane");
		maListe2.add(1,"Clement");

		System.out.println("avant suppression : ");		
		for (int i = 0; i < maListe2.size(); i++) {
			System.out.println(" � l indice : "+i+" il y a "+maListe2.get(i));
		}
		
		maListe2.clear();
		
		System.out.println("apres suppression : ");		
		for (int i = 0; i < maListe2.size(); i++) {
			System.out.println(" � l indice : "+i+" il y a "+maListe2.get(i));
		}
		
//		maListe2.get(2);

	}

}
