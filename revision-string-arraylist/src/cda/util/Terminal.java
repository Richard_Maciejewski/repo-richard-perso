package cda.util;

import java.util.Scanner;

public class Terminal {

	private static Scanner scanner = new Scanner(System.in);
	
	public static void ecrireIntln(int n) {
		System.out.println(n);
	}

	public static void ecrireStringln(String s) {
		System.out.println(s);
	}
	
	public static void ecrireBooleanln(boolean b) {
		System.out.println(b);
	}

	public static void ecrireCharln(char c) {
		System.out.println(c);
	}

	public static String lireString() {
		return scanner.nextLine();
	}

	public static void ecrireString(String s) {
		System.out.print(s);
	}

	public static int lireInt() {
		int res = scanner.nextInt();
		scanner.nextLine();
		return res;
	}

	public static double lireDouble() {
		double res = scanner.nextDouble();
		scanner.nextLine();
		return res;
	}
}
