package cda.exec;

import cda.util.Terminal;

public class EgCh {
	public static void main(String[] args){
		String s1, s2, s3;
		boolean b1, b2;
		s1 = "tati";
		s2 = "ta";
		s2 = s2 + "ti";
		s3 = s1;
		Terminal.ecrireStringln("s1=" + s1 + " s2=" + s2 + " s3=" + s3);
		b1 = (s1 == s2);
		b2 = (s1 == s3);
		Terminal.ecrireStringln("s1 == s2? " + b1);
		Terminal.ecrireStringln("s1 == s3? " + b2);
	}
}
