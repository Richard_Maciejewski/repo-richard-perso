package circle;

public class ColoredCircle extends Circle {

		private String color;
		
		
		public ColoredCircle(String color) {
			super();
			this.color = color;
		}


		@Override
		public String toString() {
			return "ColoredCircle [color=" + color + "]";
		}


		public String getColor() {
			return color;
		}


		public void setColor(String color) {
			this.color = color;
		}
		
		
}
