package circle;

public class Cylinder extends Circle {
	
		private double h;
		
	

	public Cylinder(double x, double y, double r, double h) {
			super(x, y, r);
			this.h = h;
		}

	@Override
	public String toString() {
		return "Cylinder [h=" + h + "]";
	}

	public double getH() {
		return h;
	}

	public void setH(double h) {
		this.h = h;
	}

	public double volume () {
		return this.PI * this.getRadius() * this.getRadius() * this.getH();
	}
}
