package circle;

public class Circle extends Shape {

	final static double PI = 3.141592564;
	private double radius;
	private double surface;

	public Circle() { 
		radius = 0;
		surface = 0;
	}

	public Circle(double x, double y, double r) {
		super(x, y);
		radius = r;
		surface = Math.PI * (radius * radius);
	}

//	public boolean isBigger (c1, c2) {
//
//		if(c1.getSurface() > c2.getSurface()) {
//
//			System.out.println("le cercle 1 est plus grand que le cercle 2");
//
//		}else {
//
//			System.out.println("le cercle 2 est plus grand que le cercle 1");
//
//		}
//	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + ", surface=" + surface + "]";
	}



	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getSurface() {
		return surface;
	}

	public void setSurface(double surface) {
		this.surface = surface;
	}





}
