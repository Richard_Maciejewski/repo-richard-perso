package entreprise;


import java.util.ArrayList;
import java.util.Scanner;

public class Menu {
	private static ArrayList<Employe> listeEmploye = new ArrayList<Employe>();

	public static void main(String[] args) throws Exception {

		boolean bool = true;
		int res = 0;
		Clavier monClavier = new Clavier(args.length == 0);

		while (bool) {
			menu();
			res = monClavier.saisirEntier();

			if (res == 1) {
				newEmploye();
			} else if (res == 2) {
				newResponsable();
			} else if (res == 3) {
				newCommercial();
			} else if (res == 4) {
				afficheSalaire();
			} else if (res == 5) {
				afficheTotalSalaire();
			} else if (res == 6) {
				ajoutEmployeEquipe();
			} else if (res == 7) {
				supprEmployeEquipe();
			} else if (res == 8) {
				afficheEquipeResp();
			} else if (res == 9) {
				modifIndiceSalaire();
			} else if (res == 10) {
				modifValeurSalaireEmploye();
			} else if (res == 11) {
				modifNbrVente();
			} else if (res == 12) {
				modifValeurSalaireCom();
			} else if (res == 13) {
				modifValeurIntCom();
			} else if (res == 14) {
				afficheListeEmployes();
			} else if (res == 15) {
				bool = exit();
			}
		}
	}

	private static void afficheListeEmployes() {
		for (Employe e : listeEmploye) {
			System.out.println(e);
		}
	}

	private static void menu() {
		System.out.print("1 - cr�er un employ� simple\r\n" + "2 - cr�er un responsable\r\n"
				+ "3 - cr�er un commercial\r\n" + "4 - afficher le salaire d'un employ�\r\n"
				+ "5 - afficher le total des salaires de tous les employ�s\r\n"
				+ "6 - ajouter un employ� � l'�quipe d'un responsable\r\n"
				+ "7 - supprimer un employ� d'une �quipe d'un responsable\r\n"
				+ "8 - afficher l'�quipe d'un responsable\r\n" + "9 - modifier l'indice salarial d'un employ�\r\n"
				+ "10 - modifier la valeur fixe des salaires des employ�s\r\n"
				+ "11 - modifier le nombre de vente d'un commercial\r\n"
				+ "12 - modifier la valeur du salaire fixe d'un commercial\r\n"
				+ "13 - modifier la valeur d�int�ressement des commerciaux\n" + "14 - afficher la liste des employ�s\n"
				+ "15 - sortir\n\n");
		System.out.println("Que voulez-vous faire ?");

	}

	private static boolean exit() {
		System.out.println("Au revoir !");
		return false;
	}

	private static void modifValeurIntCom() {
		System.out.println("De quel commercial voulez-vous modifier la valeur d'interessement ? (donnez son matricule)");
		Scanner sc = new Scanner(System.in);
		double res = sc.nextDouble();
		sc.nextLine();
		System.out.println("A quelle valeur voulez-vous fixer l'interessement ?");
		double interet = sc.nextDouble();
		sc.nextLine();
		for (Employe e : listeEmploye) {
			if (e.getMatricule() == res) {
				((Commercial) e).setInteret(interet);
				break;
			}
		}

	}

	private static void modifValeurSalaireCom() {
		Scanner sc = new Scanner(System.in);
		System.out.println("A quelle valeur voulez-vous fixer le salaire de base des cormmerciaux ?");
		double base = sc.nextDouble();
		sc.nextLine();
		Commercial.setBase(base);
	}

	private static void modifNbrVente() {
		System.out.println("De quel commercial voulez-vous modifier le nombre de ventes ? (donnez son matricule) ");
		Scanner sc = new Scanner(System.in);
		double res = sc.nextDouble();
		sc.nextLine();
		System.out.println("A quelle valeur voulez-vous fixer son nombre de vente ?");
		int ventes = sc.nextInt();
		sc.nextLine();
		for (Employe e : listeEmploye) {
			if (e.getMatricule() == res) {
				((Commercial) e).setVentes(ventes);
				break;
			}
		}

	}

	private static void modifValeurSalaireEmploye() {
		System.out.println("A quelle valeur voulez-vous fixer le salaire de base ?");
		Scanner sc = new Scanner(System.in);
		double base = sc.nextDouble();
		sc.nextLine();
		Employe.setBase(base);
	}

	private static void modifIndiceSalaire() {
		System.out.println("De quel employ� voulez-vous modifier l'indice salariale ? (donnez son matricule)");
		Scanner sc = new Scanner(System.in);
		double res = sc.nextDouble();
		sc.nextLine();
		System.out.println("A quelle valeur voulez-vous fixer l'indice ?");
		double indice = sc.nextDouble();
		sc.nextLine();
		for (Employe e : listeEmploye) {
			if (e.getMatricule() == res) {
				e.setIndice(indice);
				break;
			}
		}
	}

	private static void afficheEquipeResp() {
		Scanner sc = new Scanner(System.in);
		System.out.println("De quel responsable voulez-vous afficher l'�quipe ? (donnez son matricule) ");
		int responsable = sc.nextInt();
		sc.nextLine();
		for (Employe employe : listeEmploye) {
			if (employe.getMatricule() == responsable) {
				for (Employe e : ((Responsable) employe).listeSousEmployes) {
					System.out.println(e);
				}
			}
		}
	}

	private static void supprEmployeEquipe() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Quel employ� voulez-vous supprimer ? (donnez son matricule) ");
		int matriculeEmploye = sc.nextInt();
		sc.nextLine();
		System.out.println("Dans quelle equipe se trouve cet employ� ? (donnez le matricule du responsable) ");
		int responsable = sc.nextInt();
		sc.nextLine();
		for (Employe e : listeEmploye) {
			if (e.getMatricule() == matriculeEmploye) {
				for (Employe employe : listeEmploye) {
					if (employe.getMatricule() == responsable) {
						((Responsable) employe).listeSousEmployes.remove(e);
						System.out.println(" employ� supprim� ! ");
					}
				}
			}
		}
	}

	private static void ajoutEmployeEquipe() {
		Scanner sc = new Scanner(System.in);
		System.out
				.println("Dans quelle equipe voulez-vous ajouter votre employ� ? (donnez le matricule du responsable)");
		int responsable = sc.nextInt();
		sc.nextLine();
		System.out.println("Quel employ� voulez-vous ajouter � une �quipe ? (donnez son matricule) ");
		int nomEmploye = sc.nextInt();
		sc.nextLine();

		for (Employe e : listeEmploye) {
			if (e.getMatricule() == nomEmploye) {
				for (Employe employe : listeEmploye) {
					if (employe.getMatricule() == responsable) {
						if (((Responsable) employe).listeSousEmployes.contains(e)) {
							System.out.println("L'employ� int�gre d�j� l'�quipe.");
						} else {
							((Responsable) employe).listeSousEmployes.add(e);
							System.out.println(" employ� ajout� ! ");
						}
					}
				}
			}
		}
	}

	private static void afficheTotalSalaire() {
		double total = 0;
		for (Employe e : listeEmploye) {
			total = total + e.getSalaire();
		}
		System.out.println("Total des salaires des employ�s : " + total + "�");

	}

	private static void afficheSalaire() {
		System.out.println("De quel employ� voule-vous afficher le salaire ? (donnez le matricule) ");
		Scanner sc = new Scanner(System.in);
		double res = sc.nextDouble();
		sc.nextLine();
		for (Employe e : listeEmploye) {
			if (e instanceof Commercial && e.getMatricule() == res) {
				Commercial C = (Commercial) e;
				System.out.println((C.getBase() * C.getIndice()) + (C.getPrime() * C.getVentes()));
			} else if (e.getMatricule() == res) {
				System.out.println(e.getSalaire());
			}
		}
	}

	private static void newCommercial() {
		Scanner sc = new Scanner(System.in);
		System.out.println("quel est le nom du commercial ?");
		String nomCommercial = sc.nextLine();
		System.out.println("quel est l'indice ?");
		double indice = sc.nextDouble();
		sc.nextLine();
		System.out.println("quel est le mois ?");
		String mois = sc.nextLine();
		System.out.println("quel est le nombre de vente ?");
		int vente = sc.nextInt();
		sc.nextLine();
		System.out.println("quelle est sa prime ?");
		double prime = sc.nextDouble();
		sc.nextLine();
		Commercial nouveau = new Commercial(nomCommercial, indice, mois, vente, prime);
		listeEmploye.add(nouveau);
	}

	private static void newResponsable() {
		Scanner sc = new Scanner(System.in);
		System.out.println("quel est le nom du responsable ?");
		String nomResponsable = sc.nextLine();
		System.out.println("quel est l'indice ?");
		double indice = sc.nextDouble();
		sc.nextLine();
		Responsable nouveau = new Responsable(nomResponsable, indice);
		listeEmploye.add(nouveau);
	}

	private static void newEmploye() {
		System.out.println("quel est le nom ?");
		Scanner sc = new Scanner(System.in);
		String nom = sc.nextLine();
		System.out.println("quel est l'indice ?");
		double indice = sc.nextDouble();
		sc.nextLine();
		Employe nouveau = new Employe(nom, indice);
		listeEmploye.add(nouveau);
	}

}
