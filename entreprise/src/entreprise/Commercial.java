package entreprise;

public class Commercial extends Employe {

	private String mois;
	private static double base=500;
	private int ventes;
	private double prime;
	private double interet;




	public Commercial(String nom, double indice, String mois, int ventes, double prime) {
		super(nom, indice);
		this.mois = mois;
		this.ventes = ventes;
		this.prime = prime;
	}

	@Override
    public String toString() {
        return super.toString() + "Commercial [mois=" + mois + ", ventes=" + ventes + ", prime=" + prime + "]";
    }

	public String getMois() {
		return mois;
	}

	public void setMois(String mois) {
		this.mois = mois;
	}

	public int getVentes() {
		return ventes;
	}

	public void setVentes(int ventes) {
		this.ventes = ventes;
	}

	public double getPrime() {
		return prime;
	}

	public void setPrime(double prime) {
		this.prime = prime;
	}

	public double getInteret() {
		return interet;
	}

	public void setInteret(double interet) {
		this.interet = interet;
	}
	
	public static void setBase(double base) {
		Commercial.base = base;
	}

}
