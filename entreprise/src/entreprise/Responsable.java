package entreprise;

import java.util.ArrayList;

public class Responsable extends Employe {
	
	public ArrayList<Employe> listeSousEmployes;

	public Responsable(String nom, double indice) {
        super(nom, indice);
        listeSousEmployes = new ArrayList<Employe>();
    }
	
	public void ajoutListResp(Employe a) {
		listeSousEmployes.add(a);
	} 
	
	public void supprListResp(int i) {
		listeSousEmployes.remove(i);
	} 
	
	public ArrayList<Employe> getListeSousEmployes() {
		return listeSousEmployes;
	}

	public void setListeSousEmployes(ArrayList<Employe> listeSousEmployes) {
		this.listeSousEmployes = listeSousEmployes;
	}

	public void ajouterInferieursHierarchiques(Employe a) {
		for (Employe employe : listeSousEmployes) {
			if (employe instanceof Responsable) {
				System.out.println("Un employ� ne peut pas �tre son propre responsable.");			
			}
		}
		this.listeSousEmployes.add(a);
	}
	
	@Override
    public String toString() {
        return super.toString() + "Responsable [listeSousEmployes=" + listeSousEmployes + "]";
    }
	
}
