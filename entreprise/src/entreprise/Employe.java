package entreprise;

public class Employe {

	private static double base = 500;
	final private int matricule;
	private String nom;
	private double indice;
	private static int id; 
	private double salaire;


	public Employe(String nom, double indice) {
		id++;
		this.matricule = id;
		this.nom = nom;
		this.indice = indice;
		this.salaire = base * indice;
	}
	
	public double calculSalaire(double sal) {
        return this.salaire = base * indice;
    }
	
	
	public String toString() {
		return "Employe [salaire= " + base + ", matricule= " + matricule + ", nom= " + nom + ", indice= " + indice + "]";
	}

	public double getBase() {
		return base;
	}

	public static void setBase(double basep) {
		base = basep;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getIndice() {
		return indice;
	}

	public void setIndice(double indice) {
		this.indice = indice;
	}

	public int getMatricule() {
		return matricule;
	}

	public static int getId() {
		return id;
	}

	public double getSalaire() {
		return salaire;
	}
	
}
