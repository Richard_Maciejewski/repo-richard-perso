package alpha;

public class C extends A {

	protected String c;

	public C() {
		
		System.out.println("C");
		
		
	}

	@Override
	public String toString() {
		return "C [c=" + c + "]";
	}

	public String getC() {
		return c;
	}

	public void setC(String c) {
		this.c = c;
	}
	
	
	
}
