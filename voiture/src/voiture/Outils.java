package voiture;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;


public  class Outils {


	//******************************************************************
	// String[] --> ArrayList

	public static ArrayList<String> A2AL(String[] tab) {

		ArrayList<String> arr = new ArrayList<String>();
		for(int i=0; i<tab.length;i++) {
			arr.add(tab[i]);
		}

		return arr;
	}

	//******************************************************************
	// ArraList --> String[]

	public static String[] AL2A(ArrayList<String> arr) {

		String[] tab = new String[arr.size()];

		for(int i=0; i<arr.size(); i++) {
			tab[i]=arr.get(i);
		}
		return tab;
	} 


	//******************************************************************
	// trie d'une HashMap, a revoir.

	public static HashMap<Voiture, String> trieHM(HashMap<Voiture,String> hm){
		// creation d'une arraylist
		ArrayList<Voiture> arr = new ArrayList<Voiture>();
		// creation d'un iterator pour la recuperation de la keyset de hm
		Iterator<Voiture> ite = hm.keySet().iterator();
		//recuperation de chaque key de la hm dans la arraylist
		while(ite.hasNext()) {
			Voiture car = ite.next();

			arr.add(car);
		}
		// trie de la arraylist de voiture, a l'aide de la methode compareTo de la classe voiture
		//arr.sort(null); // peut etre que la methode sort() ne fait pas se que lon souhaite donc il faut revoir cette partie
		//recuperation de la arraylist trie et creation d'une nouvelle hm avex la nouvelle keyset trie
		int tail = arr.size();
		
				for(int i = 0 ; i < tail - 1 ; i++) {
					Voiture v1 = arr.get(i);
					Voiture v2 = arr.get(i + 1);
					boolean z = v1.compareTo(v2);
					if (z) {
						Voiture temp = v2;
						v2 = v1;
						v1 = temp;
						arr.set(i, v1);
						arr.set(i + 1, v2);
										
						
					}
					
				}
			
		ArrayList<String> list = new ArrayList<String>();
			for(int i = 0 ; i < tail ; i++) {
				
				String s = hm.get(i);
				list.add(s);
			}
			return list.;
				
				
		HashMap<Voiture,String> hm2 = new HashMap<Voiture,String>();
		for (int i = 0; i < arr.size(); i++) {
			hm2.put(arr.get(i), hm.get(arr.get(i)));
		}

		return hm2;
		
	}

	//public static void trierVoiture 
	
	
	
	
	//******************************************************************
	// HashMap --> String[]

	public static String[] HM2A(HashMap<Voiture,String> hm) {

		String[] tab = new String[hm.size()];
		hm = Outils.trieHM(hm);

		Iterator<Voiture> ite = hm.keySet().iterator();
		int i = 0;
		while(ite.hasNext()) {
			Voiture car = ite.next();
			tab[i] = hm.get(car);
			i++;
		}

		return tab;
	}



	//******************************************************************
	// String[] --> HashMap

	public static HashMap<Integer,String> A2HM(String[] tab) {

		HashMap<Integer,String> hm = new HashMap<Integer,String>();
		Arrays.sort(tab);
		for(int i=0; i<tab.length; i++) {

			hm.put(i, tab[i]);

		}
		return hm;
	}




	//******************************************************************
	//HashMap ---> Arrays

	public static ArrayList<String> HM2AL(HashMap<Voiture,String> hm) {

		ArrayList<String> arr = new ArrayList<String>();
		hm = Outils.trieHM(hm);

		Iterator<Voiture> ite = hm.keySet().iterator();
		while(ite.hasNext()) {
			Voiture car = ite.next();
			arr.add(hm.get(car));
		}


		return arr;
	}

}
