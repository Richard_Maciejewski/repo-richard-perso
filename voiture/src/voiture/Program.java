package voiture;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Arrays;

public class Program {
    public static void main(String[] args) {
        String[] noms = { "Renault", "Peugeot", "Porsche" };
        Outils.A2AL(noms);
        for (int i = 0; i < noms.length; i++) {
            System.out.println(noms[i]);
        }
        ArrayList<String> a = new ArrayList<String>();
        a.add("Volkswagen");
        a.add("BMW");
        a.add("Audi");
        System.out.println("ArrayList : " + a);
        String[] tab = Outils.AL2A(a);
        System.out.println("tableau de String[] : " + Arrays.toString(tab));
        
        HashMap<Voiture, String> maHm = new HashMap<Voiture, String>();
        maHm.put(new Voiture("BMW","X5",2018),"a");
        maHm.put(new Voiture("AUDI","A5",2016),"b");
        maHm.put(new Voiture("PORSCHE","911",2017),"c");
        
        Outils.trieHM(maHm);
        System.out.println("Voitures trie : " + maHm);
    }
}
