package promoCda;

import java.util.HashSet;

public class Promo {

	private HashSet<Eleve> listEleves;
	private final String nom;
	private static int id = 1;
	private final int promoId;

	public Promo(String nomEleve) {
		this.listEleves = new HashSet<Eleve>();
		this.promoId = id;
		Promo.id++;
		this.nom = nomEleve;

	}

	public HashSet<Eleve> getlistEleves() {
		return listEleves;
	}

	public void setlistEleves(HashSet<Eleve> listEleves) {
		this.listEleves = listEleves;
	}

	public String getNom() {
		return this.nom;
	}

	public String toString() {
		return "Promotion " + this.promoId + this.nom;
	}

	public Integer getPromoId() {
		return promoId;
	}



	public void setListEleves(HashSet<Eleve> listEleves) {
		this.listEleves = listEleves;
	}

	public void supprimerEleve(String nom) {
		Eleve recherche = null;
		boolean elevePareil = false;
		for (Eleve eleves : listEleves) {
			if(eleves.getNom().equalsIgnoreCase(nom)) {
				elevePareil = true;
				recherche = eleves;
			}
		}
		if(elevePareil) {
			listEleves.remove(recherche);
			System.out.println("La promotion " + nom + " existe bien et a �t� supprim�e");
		}
	}

	public void ajouterEleve(String nom) {
		boolean elevePareil = false;
		for (Eleve eleves : listEleves) {
			if(eleves.getNom().equalsIgnoreCase(nom)) {
				elevePareil = true;
				System.out.println("L'�tudiant existe deja");
				break;
			}
		}
		if(elevePareil == false) {
			Eleve eleve = new Eleve(nom);
			listEleves.add(eleve);
			System.out.println("L'�tudiant a bien ete ajoute");
		}
	}



}
