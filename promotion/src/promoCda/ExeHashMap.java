package promoCda;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ExeHashMap {
   
	public static void main(String[] args) {
       
		HashMap<String, ArrayList<Integer>> notes = new HashMap<String, ArrayList<Integer>>();
       notes.put("math", new ArrayList<Integer>());
       notes.put("geo", new ArrayList<Integer>());
       notes.put("info", new ArrayList<Integer>());
       notes.get("math").add(14);
       notes.get("math").add(15);
       notes.get("geo").add(10);
       notes.get("info").add(18);
       notes.get("info").add(19);
       notes.get("info").add(18);
       notes.get("info").add(12);
       System.out.println(notes);
       Iterator<String> iterator = notes.keySet().iterator();
       
       while(iterator.hasNext()) {
           String laMatiere = iterator.next();
           ArrayList<Integer> listeDesNotes = notes.get(laMatiere);
           double total = 0;
           for(Integer n : listeDesNotes) {
               total+=n;
           }
           System.out.println("la moyenne pour "+laMatiere+" est "+(total/listeDesNotes.size()));
       }
   }
}
