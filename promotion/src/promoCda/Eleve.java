package promoCda;

import java.util.TreeMap;
import java.util.TreeSet;

public class Eleve {


	private static int id;

	private int monid;
	private String nom = "";
	private String prenom = "";
	//public String matiere = "";

	//TreeSet de type Integer
	private TreeMap<Integer , String> tabEleves = new TreeMap<Integer , String>();

	public Eleve(int monid, String nom, String prenom) {
		this.monid = monid;
		this.nom = nom;
		this.prenom = prenom;
		id++;
	}

	public Eleve() {
		this.monid = monid;
		this.nom = nom;
		this.prenom = prenom;
		id++;
	}

	public Eleve(String nom) {
		this.monid = monid;
		this.nom = nom;
		this.prenom = prenom;
		id++;
	}

	public String getNom() {
		return nom;
	}
	public int getMonid() {
		return monid;
	}
	public void setMonid(int monid) {
		this.monid = monid;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public static int getId() {
		return id;
	}
	public static void setId(int id) {
		Eleve.id = id;
	}

	public TreeMap<Integer, String> getTabEleves() {
		return tabEleves;
	}

	public void setTabEleves(TreeMap<Integer, String> tabEleves) {
		this.tabEleves = tabEleves;
	}


}
