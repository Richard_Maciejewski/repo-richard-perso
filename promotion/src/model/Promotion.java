package model;



public class Promotion {
		
	
		private String nom;
		
		
		public Promotion(String nomPromotion) {
			this.nom = nomPromotion;		
		}
		
		public String getNom() {
			return this.nom;
		}

		public String toString() {
			return "Promotion " + this.nom;
		}


}		
