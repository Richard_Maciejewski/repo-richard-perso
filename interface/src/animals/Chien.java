package animals;

public class Chien extends Animal implements Criant, Marche, Nage {

	@Override
	public void quiNage() {
		System.out.println("le chien nage");
		
	}

	@Override
	public void quiMarche() {
		System.out.println("le chien marche");
		
	}

	@Override
	public void afficherCri() {
		System.out.println("le chien aboie");
		
	}

	@Override
	public void quiRespire() {
	
		super.quiRespire();
	}
	
		
		
		
}
