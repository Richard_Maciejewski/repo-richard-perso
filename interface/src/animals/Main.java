package animals;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		Chat minou = new Chat();
		Chat felix = new Chat();
		Chat minet = new Chat();
		Chien bob = new Chien();
		Chien bruce = new Chien();
		Poisson chleo = new Poisson();

		ArrayList<Animal> listAnimaux = new ArrayList<Animal>();

		listAnimaux.add(minou);
		listAnimaux.add(felix);
		listAnimaux.add(minet);
		listAnimaux.add(bob);
		listAnimaux.add(bruce);
		listAnimaux.add(chleo);

		for (Animal animal : listAnimaux) {
			animal.quiRespire();
			if (animal instanceof Marche) {
				((Marche) animal).quiMarche();
			}
			if (animal instanceof Nage) {
				((Nage) animal).quiNage();
			}

			if (animal instanceof Criant) {
				((Criant) animal).afficherCri();
			}


		}
	}

	@Override
	public String toString() {
		return "Main [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
		+ "]";
	}

}
